<?php
include 'php/connect.php';
session_start();
if(!$_SESSION['LoggedIn']){
	header("location:index.php");
}else{
	if(!isset($_GET['user']) || empty($_GET['user'])){
		$sql = "SELECT taker,sender FROM chat WHERE taker={$_SESSION['UserID']} OR sender={$_SESSION['UserID']} ORDER BY date DESC LIMIT 1";
		$result = $connection->query($sql);
		$row = $result->fetch_assoc();
		if($result->num_rows >0){
			if($row['taker'] != $_SESSION['UserID']){
				header("location:chat.php?user={$row['taker']}");
			}else{
				header("location:chat.php?user={$row['sender']}");
			}
		}else{
			header("location:chat.php?user=false");
		}
	}
	include 'php/log.php';
	logTheActivity('chat');
}

?>
<!DOCTYPE html>
<html>
<head lang="hu">
<meta charset="utf-8"></meta>
<title>Szaki.hu</title>
<!--bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!--general css -->
<link rel="stylesheet" href="css/style.css">
<!--site css-->
<link rel="stylesheet" href="css/chat.css">
<!--fontawesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body data='<?php echo $_SESSION["UserID"]; ?>'>
<!--navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="loggedin.php">Szaki.hu</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-bars fa-2x" id="close" style="color:white;"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="loggedin.php">Főoldal</a>
      </li>
	   <li class="nav-item active">
        <a class="nav-link" href="search.php">Keresés</a>
     </li>
     <li class="nav-item active">
        <a class="nav-link" href="#">Társalgó</a>
     </li>

    </ul>
    <ul class="navbar-nav mr-end">
      <li class="nav-item active">
        <a class="nav-link" href="profile.php">Profil</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="php/logout.php">Kilépés</a>
      </li>

    </ul>
  </div>
</nav>
<section class="main row">
		<div id="background"></div>
		<div id="bigImageDiv" >
			<div id="bigImageInsideDiv" class="thumbnail">
				<img id="bigImage"></img>
				<i id="previousPicture" onclick="previousPicture()" class="imageHud fas fa-3x fa-chevron-circle-left"></i>
				<i id="nextPicture" onclick="nextPicture()" class="imageHud fas fa-3x fa-chevron-circle-right"></i>
				<i id="closePicture" onclick="CloseBigImage()" class="imageHud fas fa-lg fa-times"></i>
			</div>

		</div>
    <div class="col-3" id="onlineDiv">
			<div class="header">
				<h5>Üzeneteim</h5>
                <i id="close" onclick="CloseOnlineDiv()" class="fas fa-angle-left fa-3x"></i>
			</div>
						<?php

								if (isset($_GET['user']) && !empty($_GET['user']) && $_GET['user']) {
									$users =array();
									$id = $_SESSION['UserID'];
									$sqlUserid = "SELECT * FROM chat WHERE taker = $id OR sender = $id GROUP BY sender,taker";
									$resultUserid = $connection->query($sqlUserid);
									while ($row = $resultUserid->fetch_assoc()) {
										if(!in_array($row['taker'],$users) && $row['taker']!=$id){
											array_push($users,$row['taker']);
										}
										if(!in_array($row['sender'],$users) && $row['sender']!=$id){
											array_push($users,$row['sender']);
										}
									}
									foreach ($users as $user) {
										$sqlUser = "SELECT users.userID AS ID,first_name,last_name,profile_image,job_name
																FROM users LEFT JOIN contractors ON users.userID = contractors.userID
																WHERE users.userID = $user";
										$resultUser = $connection->query($sqlUser);
										$row = 	$resultUser->fetch_assoc();
										echo "<a class='onlineUserLink' href='chat.php?user={$row['ID']}'>
				                		<div class='onlineUserDiv'>
					                  	<img src='{$row['profile_image']}'></img>
					                  	<p>{$row['first_name']} {$row['last_name']}
															";
										if(!empty($row['job_name'])) echo"({$row['job_name']})";
										echo			"<span></span></p>
				                		</div>
	 												</a>";
									}
								}
					 	?>
		</div>
    <div class="col-9" id="chatDiv">
        <div class="header">
						<button class="btn" id="onlineUserToggleBtn"><i class="fas fa-bars fa-2x" ></i></button>
            <h5>
                <?php
								if (isset($_GET['user']) && !empty($_GET['user']) && $_GET['user']) {
	                $getSenderSql = "SELECT first_name,last_name FROM users WHERE userID = {$_GET['user']}";
	                $getSenderResult= $connection->query($getSenderSql);
	                $row = $getSenderResult->fetch_assoc();
	                echo $row['first_name']." ".$row['last_name'];
							  }
                ?>
            </h5>
        </div>
        <div id="messagesDiv">
					<div class="message left">

						<p <p class="messageDate"></p>
						<div >

						<p class="messageText">
							Önnek jelenleg egyetlen üzente sincs.Keressen vállalkozót a "<a id='searchLink' href="search.php">Keresés</a>" oldalon és kérjen árajánlatot.</p>
						</div>
					</div>
        </div>
        <div class="align-self-end w-100" id="sendMessageDiv">
							<div class="input-group row">
  							<textarea class="form-control col-12" name="message"></textarea>
								<label id="uploadImage" for="uploadImageInput"><i class="fas fa-image fa-2x"></i></label>
								<input id="uploadImageInput" type="file" name="image"></input>
								<div class="input-group-prepend">
    							<button class="btn btn-primary" name="taker" <?php
									if (isset($_GET['user']) && !empty($_GET['user']) && $_GET['user']) {
										if($_GET['user'] == "false"){
											echo "value='".$_SESSION['UserID']."'";
										}else {
											echo "value='".$_GET['user']."'";
										}
    							}
									?> >Küldés</button>
  							   </div>
							</div>
        </div>
    </div>

</section>
</body>
<!--jquery -->
<script src="js\Bootstrap\Popper.js" type="text/javascript"></script>
<script src="js\jQuery\jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js\Bootstrap\bootstrap.min.js" type="text/javascript"></script>
<script src="js/chat.js" type="text/javascript"></script>
</html>
