<?php
include 'php/connect.php';
session_start();
if(isset($_SESSION['LoggedIn'])){
	header("location:loggedin.php");
}

?>
<!DOCTYPE html>
<html>
<head lang="hu">
<meta charset="utf-8"></meta>
<title>Szaki.hu</title>
<!--bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!--saját -->
<link rel="stylesheet" href="css/style.css">
<!--fontawesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<style>
.main{
	padding-top: 10px;
}
/*alert box*/
.alert-danger{
  display:block;
  opacity: 1;
  position: fixed;
	margin:auto;
  color: white;
	z-index: 1051;
	left: 50%;
  right: 50%;
	width: 70%;
	transform: translate(-50%);
  background-color: #de1a1a;
  border-color: #780116;
}
.close{
  text-shadow: none;
}
</style>
</head>
<body>
<!--navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="index.php">Szaki.hu</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-bars fa-2x" style="color:white;"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Főoldal</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Regisztráció</a>
      </li>


    </ul>
  </div>
</nav>

<section class=" main form-section">
<div id="contianer">
	<?php
		if(isset($_GET['message']) AND !empty($_GET['message'])){
			echo
			"<div  class='alert alert-danger alert-dismissible fade show'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Hiba regisztráció során!</strong>
					{$_GET['message']}
		</div>";
		}

	 ?>
<form method="POST" action="php/signup.php">
	<div class="form-group row">
		<div class="btn-group btn-group-toggle col-sm-3" data-toggle="buttons">
			<label class="btn btn-secondary radio-btn <?php if(!isset($_SESSION["category"]) || $_SESSION["category"] != "contractor" ) echo 'active' ?>">
				<input type="radio" name="category" value="individual" id="individual" autocomplete="off" <?php if(!isset($_SESSION["category"]) || $_SESSION["category"] != "contractor" ) { ?> checked <?php } ?>  onchange="change('none')"> Magánszemély
			</label>
			<label class="btn btn-secondary radio-btn <?php if(isset($_SESSION["category"]) && $_SESSION["category"] == "contractor" ) echo 'active' ?>">
				<input type="radio" name="category" value="contractor" id="contractor" autocomplete="off" <?php if(isset($_SESSION["category"]) && $_SESSION["category"] == "contractor" ) { ?> checked <?php } ?> onchange="change('flex')"> Válalkozó
			</label>
		</div>
	</div>
  <div class="form-group row">
    <label for="inputLastName" class="col-sm-3 col-form-label">Vezetéknév </label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputLastName" name="inputLastName" placeholder="Vezetéknév" onfocus="checkName('inputLastName',0)" onfocusout="checkName('inputLastName',0),dismissPopover('inputLastName')" onkeyup="checkName('inputLastName',0)"
			value="<?php if(isset($_SESSION['LastName'])) echo $_SESSION['LastName']; ?>" data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
			data-content=
			"<ul>
			  <li>Nagybetűvel kell kezdödnie</li>
			  <li>Csak betűt tartalmazhat</li>
			  <li>Nem tartalmazhat számot</li>
		  </ul>"
		  >
		<span class="form-icon" id="inputLastNameIcon"><i class="fas fa-exclamation"></i></span>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputFirstName" class="col-sm-3 col-form-label">Keresztnév </label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputFirstName" name="inputFirstName" placeholder="Keresztnév" onfocus="checkName('inputFirstName',1)" onfocusout="checkName('inputFirstName',1),dismissPopover('inputFirstName')" onkeyup="checkName('inputFirstName',1)"
			value="<?php if(isset($_SESSION['FirstName'])) echo $_SESSION['FirstName']; ?>"
			data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
			data-content=
				  "<ul>
					  <li>Nagybetűvel kell kezdödnie</li>
					  <li>Csak betűt tartalmazhat</li>
					  <li>Nem tartalmazhat számot</li>
				  </ul>"
		  >
			<span class="form-icon" id="inputFirstNameIcon"><i class="fas fa-exclamation"></i></span>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputEmail" class="col-sm-3 col-form-label">Email </label>
    <div class="col-sm-9">
      <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email"  onfocus="checkEmail('inputEmail',2)" onfocusout="checkEmail('inputEmail',2),dismissPopover('inputEmail')" onkeyup="checkEmail('inputEmail',2)"
			value="<?php if(isset($_SESSION['Email'])) echo $_SESSION['Email']; ?>"
			data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
			data-content=
					  "<ul>
						  <li>Tartalmaznia kell '@' karaktert</li>
						  <li>Tartalmaznia kell '.' karaktert</li>
					  </ul>"
			>
			<span class="form-icon" id="inputEmailIcon"><i class="fas fa-exclamation"></i></span>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-3 col-form-label">Jelszó </label>
    <div class="col-sm-9 ">
      <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Jelszó" onfocus="checkPassword('inputPassword',3)" onfocusout="checkPassword('inputPassword',3),dismissPopover('inputPassword')" onkeyup="checkPassword('inputPassword',3)"
			data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
			data-content=
						"<ul>
							<li>Minimum 9  karakter hoszúnak kell lennie </li>
							<li>Tartalmaznia kell speciális karaktert</li>
							<li>Tartalmaznia kell kis- és nyagybetüt</li>
							<li>Tartalmaznia kell számot</li>
						</ul>"
			>
			<span class="form-icon" id="inputPasswordIcon"><i class="fas fa-exclamation"></i></span>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPasswordAgain" class="col-sm-3 col-form-label">Jelszó újra </label>
    <div class="col-sm-9">
			<input type="password" class="form-control" id="inputPasswordAgain" placeholder="Jelszó mégegyszer" onfocus="checkPassword('inputPasswordAgain',4)" onfocusout="checkPassword('inputPasswordAgain',4),dismissPopover('inputPasswordAgain')" onkeyup="checkPassword('inputPasswordAgain',4)"
			data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
			data-content=
						"<ul>
							<li>Minimum 9  karakter hoszúnak kell lennie </li>
							<li>Tartalmaznia kell speciális karaktert</li>
							<li>Tartalmaznia kell kis- és nyagybetüt</li>
							<li>Két jelszónak meg kell egyezniük</li>
							<li>Tartalmaznia kell számot</li>
						</ul>"
			>
			<span class="form-icon" id="inputPasswordAgainIcon"><i class="fas fa-exclamation"></i></span>
    </div>
  </div>
  <div class="form-group row" id="divJobTitle" style="display:none;">
    <label for="inputJobTitle" class="col-sm-3 col-form-label">Szakma megnevezése</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputJobTitle" name="inputJobTitle" placeholder="Szakma megnevezése" value="<?php if(isset($_SESSION['JobTitle'])) echo $_SESSION['JobTitle']; ?>">
    </div>
  </div>


  <div class="form-group row">
    <div class="col-sm-6">
      <button type="submit" class="btn btn-secondary" id="registerButton" disabled>Regisztráció</button>
    </div>
  </div>
</form>
</div>
</section>


</body>
<script src="js\Bootstrap\Popper.js" type="text/javascript"></script>
<script src="js\jQuery\jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js\Bootstrap\bootstrap.min.js" type="text/javascript"></script>
<script src="js/validator.js" type="text/javascript"></script>
<script src="js/registerTDD.js" type="text/javascript"></script>
<script src="js/register.js" type="text/javascript"></script>
</html>
