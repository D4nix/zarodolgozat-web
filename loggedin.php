<?php
include 'php/connect.php';
session_start();
if(!$_SESSION['LoggedIn']){
	header("location:index.php");
}

include 'php/log.php';
logTheActivity('loggedin');
?>
<!DOCTYPE html>
<html>
<head lang="hu">
<meta charset="utf-8"></meta>
<title>Szaki.hu</title>
<!--bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!--general css -->
<link rel="stylesheet" href="css/style.css">
<!--site css-->
<link rel="stylesheet" href="css/loggedin.css">
<!--fontawesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body data="<?php echo $_SESSION['UserID']; ?>">
<!--navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Szaki.hu</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-bars fa-2x" style="color:white;"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Főoldal</a>
      </li>
	    <li class="nav-item active">
        <a class="nav-link" href="search.php">Keresés</a>
      </li>
			<li class="nav-item active">
				 <a class="nav-link" href="chat.php">Társalgó</a>
			</li>

    </ul>
    <ul class="navbar-nav mr-end">
      <li class="nav-item active">
        <a class="nav-link" href="profile.php">Profil</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="php/logout.php">Kilépés</a>
      </li>

    </ul>
  </div>
</nav>
<?php
if($_SESSION['category'] == 'contractor'){
	$sql = 'SELECT job_name,job_title,state,description
					FROM contractors
					WHERE contractors.userID='.$_SESSION["UserID"];
	$result = $connection->query($sql);
	$row = $result->fetch_assoc();
	$show ="";
	foreach ($row as $data) {
		if(empty($data)) $show = "show";
	}
}
?>
<section id="welcome" class=" main row justify-content-center ">
	<div  class='alert alert-warning alert-dismissible fade <?php echo $show; ?>'>
	    <a href='#' class='close' data-dismiss="alert" aria-label='close'>&times;</a>
	    <strong id="alertBoxMessage">Figyelmeztetés</strong>
			<span>Kérjük töltse ki a vállalkozás adatait a jobb keresés érdekében.<a href="profile.php" class="badge badge-light">Ide kattintva megteheti</a></span>
	</div>
	<h2 class=" col-sm-8 align-self-center section-text form-group">
		<label for="searchValue">Szakembert keres?</label>
		<input class="form-control col-5 mx-auto mb-1" id="searchValue"></input>
		<button class="btn btn-secondary mx-auto mb-1" id="searchBtn">Keresés</button>
	</h2>
</section>
<section>
	<div>
		<h2 class="text-center">A legjobb vállalkozók</h2>
	<div>
	<div class="d-flex flex-wrap mx-auto h-50">
		<?php
			$sql = "SELECT users.userID,first_name,last_name,profile_image,job_title,COALESCE(SUM(likes.liked),0)  / COALESCE(COUNT(likes.contractorID),0)*100 AS 'rating'
							FROM users INNER JOIN contractors ON users.userID = contractors.userID INNER JOIN likes ON contractors.userID = likes.contractorID
							GROUP BY users.userID
							ORDER BY rating DESC
							LIMIT 3";
			$result = $connection->query($sql);
			if($result->num_rows > 0){
				while ($row = $result->fetch_assoc()) {
					$percentage = round($row['rating'] , 2);
					if($percentage <= 40) $class = 'bad';
					if($percentage > 40 && $percentage <= 80) $class = 'avg';
					if($percentage > 80) $class = 'good';
					echo '<div class="card col-4 col-lg-3 mx-auto" data-toggle="modal" data-target="#showContractorModal" name="'.$row['userID'].'">
									<img class="card-img-top" src="'.$row["profile_image"].'" alt="Profilkép" style="width:100%">
									<div class="card-body">
										<h4 class="card-title">'.$row["first_name"].' '. $row["last_name"].'</h4>
										<h6 class="card-title">'.$row["job_title"].'</h6>
										<div>
											<span class="float-right '.$class.'" style="padding-top: calc(.375rem + 1px);padding-bottom: calc(.375rem + 1px);">'.$percentage.'%</span>
										</div>
									</div>
								</div>';
				}
			}else{
				echo '<div class="card col-4 col-lg-3 mx-auto">
								<div class="card-body">
									<h4 class="card-title">Sajnáljuk!</h4>
									<p class="card-text">Nincs értékelt vállalkozó</p>
								</div>
							</div>';
			}
		 ?>
	</div>
</section>
<section>
	<div>
		<h2 class="text-center">A legrosszabb vállalkozók</h2>
	<div>
	<div class="d-flex flex-wrap mx-auto h-50">
		<?php
			$sql = "SELECT users.userID,first_name,last_name,profile_image,job_title,COALESCE(SUM(likes.liked),0)  / COALESCE(COUNT(likes.contractorID),0)*100 AS 'rating'
							FROM users INNER JOIN contractors ON users.userID = contractors.userID INNER JOIN likes ON contractors.userID = likes.contractorID
							GROUP BY users.userID
							ORDER BY rating
							LIMIT 3";
			$result = $connection->query($sql);
			if($result->num_rows > 0){
				while ($row = $result->fetch_assoc()) {
					$percentage = round($row['rating'] , 2);
					if($percentage <= 40) $class = 'bad';
					if($percentage > 40 && $percentage <= 80) $class = 'avg';
					if($percentage > 80) $class = 'good';
					echo '<div class="card col-4 col-lg-3 mx-auto" data-toggle="modal" data-target="#showContractorModal" name="'.$row['userID'].'">
									<img class="card-img-top" src="'.$row["profile_image"].'" alt="Profilkép" style="width:100%">
									<div class="card-body">
										<h4 class="card-title">'.$row["first_name"].' '. $row["last_name"].'</h4>
										<h6 class="card-title">'.$row["job_title"].'</h6>
										<div>
											<a href="#" class="btn btn-primary">See Profile</a>
											<span class="float-right '.$class.'" style="padding-top: calc(.375rem + 1px);padding-bottom: calc(.375rem + 1px);">'.$percentage.'%</span>
										</div>
									</div>
								</div>';
				}
			}else{
				echo '<div class="card col-4 col-lg-3 mx-auto">
								<div class="card-body">
									<h4 class="card-title">Sajnáljuk!</h4>
									<p class="card-text">Nincs értékelt vállalkozó</p>
								</div>
							</div>';
			}
		 ?>
	</div>
</section>
<!-- show contractor info modal -->
<div class="modal fade" id="showContractorModal" tabindex="-1" role="dialog" aria-labelledby="contractorModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
            <div class="modal-header">
		<h5 class="modal-title" id="JobName">Vállalkozás adatai</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
		</button>
            </div>
            <div class="modal-body">

							<div class="form-group row">
									<div class="col-sm-4 ">
										<div class="profileImage thumbnail" id="profileImage"  style="height:155px;"></div>
										<p class="col col-form-label justify-content-center form-group row m-0">
											<a class="col-5 btn" id="like-btn"><i class="fas fa-thumbs-up"></i><span id="like-btn-display"></span></a>
											<a class="col-5 btn" id="dislike-btn"><i class="fas fa-thumbs-down"></i><span id="dislike-btn-display"></span></a>
										</p>
										<h5 id="Name" class="col"></h5>
									</div>
									<div class="col-sm-8 form-group row">
											 <h4 id="JobTitle" class="col-12"></h4>
											 <p id="JobState" class="col-12 col-form-label"></p>
											 <p id="JobDescription" class="col col-form-label"></p>
									</div>
							</div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-secondary" id="sendMessage" href="chat.php" type="button">Üzenet küldése</a>
            </div>
	</div>
    </div>
</div>
</body>
<!--jquery -->
<script src="js\Bootstrap\Popper.js" type="text/javascript"></script>
<script src="js\jQuery\jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js\Bootstrap\bootstrap.min.js" type="text/javascript"></script>
<script src="js/loggedin.js" type="text/javascript"></script>
</html>
