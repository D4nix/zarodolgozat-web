<?php
include 'php/connect.php';
session_start();
if(!$_SESSION['LoggedIn']){
	header("location:index.php");
}
include 'php/log.php';
logTheActivity('search');
if(empty($_GET)){
	$url = $_SERVER["REQUEST_URI"]."?page=";
	$page = 1;
}else{
	if(isset($_GET['page'])){
		if(!empty($_GET['page'])){
			$url = substr_replace($_SERVER["REQUEST_URI"], "", -1);
		}else{
			$url = $_SERVER["REQUEST_URI"];
		}
		$page = $_GET['page'];
	}else{
		$url = $_SERVER["REQUEST_URI"]."&page=";
		$page = 1;
	}
}
?>
<!DOCTYPE html>
<html>
<head lang="hu">
<meta charset="utf-8"></meta>
<title>Szaki.hu</title>
<!--bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!-- general css -->
<link rel="stylesheet" href="css/style.css">
<!--site css-->
<link rel="stylesheet" href="css/search.css">
<!--fontawesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body data="<?php echo $_SESSION['UserID']; ?>">
<!--navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="index.php">Szaki.hu</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-bars fa-2x" style="color:white;"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="loggedin.php">Főoldal</a>
      </li>
	  	<li class="nav-item active">
        <a class="nav-link" href="#">Keresés</a>
      </li>
			<li class="nav-item active">
				 <a class="nav-link" href="chat.php">Társalgó</a>
			</li>

    </ul>
	<ul class="navbar-nav mr-end">
		<li class="nav-item active">
			<a class="nav-link" href="profile.php">Profil</a>
		</li>
    <li class="nav-item active">
        <a class="nav-link" href="php/logout.php">Kilépés</a>
    </li>

  </ul>
  </div>
</nav>
<section class="main row">
<div id="searchBar" class="col-12 col-xl-3 form-section">
	<form action="php/searchContractors.php" method="POST">
		<label for="inputSearchJobTitle" class="col col-form-label"><h5>Keresés</h5></label>
		<div class="form-group row col">
			  <input type="text" class="form-control" id="inputSearchJobTitle" name="inputSearchJobTitle" placeholder="Szakma keresése"
				<?php if (isset($_GET['search']) AND $_GET['search'] == 'job_title') {
			  	echo "value='".$_GET['searchValue']."'";
			  }?>>
		</div>
		<h5>Rendezés</h5>

			<div class="form-group row col pr-0 orderDiv">
					<label  for="inputOrderJobTitle">
						<input type="radio" class="orderRadioBtn" id="inputOrderJobTitle" <?php if(!isset($_GET['order']) OR $_GET['order']=='job_title') echo 'checked' ?> name="order" value="job_title" >
						<span class="orderRadioBtnIcon"></span>
					</label>
				<label class="orderMainLabel col-9" for="inputOrderJobTitle">Szakma szerint</label>
				<label class="custom-checkbox" >
					<input class="orderCheckbox" type="checkbox" id="checkboxOrderJobTitle"
					<?php
						if(isset($_GET['order']) AND $_GET['order']!='job_title') echo 'disabled';
						else {
							if(isset($_GET['orderValue']) AND $_GET['orderValue'] == 'DESC') {echo 'checked '; }
							echo 'name="checkboxOrder"';
						}
					?>
					>
					<span class="orderCheckboxIcon"></span>
				</label>
			</div>

			<div class="form-group row col pr-0 orderDiv">
				<label  for="inputOrderJobName">
						<input type="radio" class="orderRadioBtn" id="inputOrderJobName" <?php if(isset($_GET['order']) AND $_GET['order']=='job_name') echo 'checked' ?>  name="order" value="job_name" >
						<span class="orderRadioBtnIcon"></span>
				</label>
				<label class="orderMainLabel col-9" for="inputOrderJobName">Vállalkozások szerint</label>
				<label class="custom-checkbox">
					<input class="orderCheckbox" type="checkbox" id="checkboxOrderJobName"
					<?php
						if(!isset($_GET['order']) OR $_GET['order']!='job_name') echo 'disabled';
						else {
							if($_GET['orderValue'] == 'DESC') {echo 'checked '; }
							echo 'name="checkboxOrder"';
						}
					?>
					>
					<span class="orderCheckboxIcon"></span>
				</label>
			</div>

			<div class="form-group row col pr-0 orderDiv">
				<label  for="inputOrderRating">
						<input type="radio" class="orderRadioBtn" id="inputOrderRating" <?php if(isset($_GET['order']) AND $_GET['order']=='rating') echo 'checked' ?>  name="order" value="rating" >
						<span class="orderRadioBtnIcon"></span>
				</label>
				<label class="orderMainLabel col-9" for="inputOrderRating">Értékelés szerint</label>
				<label class="custom-checkbox">
				  <input class="orderCheckbox" type="checkbox" id="checkboxOrderRating"
					<?php
						if(!isset($_GET['order']) OR $_GET['order']!='rating') echo 'disabled';
						else {
							if($_GET['orderValue'] == 'DESC') {echo 'checked '; }
							echo 'name="checkboxOrder"';
						}
					?>
					>
				  <span class="orderCheckboxIcon"></span>
				</label>
			</div>

			<div class="form-group row col justify-content-end">
				<button name="searchBtn" class='btn btn-primary'>Keresés</button>
			</div>

	</form>
</div>
<div class="col-11 col-lg-9 d-flex flex-wrap mx-auto" id="contractors">
	<?php
		$sql = "SELECT users.userID,job_name,job_title, description,
					COALESCE(state_name,'') AS 'state_name', profile_image,
					COALESCE(COUNT(likes.contractorID),0) AS 'allLikes' ,
					COALESCE(SUM(likes.liked),0) AS 'likes',
					COALESCE(SUM(likes.liked),0)  / COALESCE(COUNT(likes.contractorID),0)*100 AS 'rating'
					FROM contractors INNER JOIN users ON contractors.userID = users.userID LEFT JOIN states ON contractors.state = states.id LEFT JOIN likes ON contractors.userID = likes.contractorID
					GROUP BY users.userID";
		if(isset($_GET['search']) AND !empty($_GET['search']) AND isset($_GET['searchValue']) AND !empty($_GET['searchValue'])){
			$search = mysqli_real_escape_string($connection,$_GET['search']);
			$value =  mysqli_real_escape_string($connection,$_GET['searchValue']);
			$sql.=" HAVING $search LIKE '%$value%'";
		}
		if(isset($_GET['order']) AND !empty($_GET['order']) AND isset($_GET['orderValue']) AND !empty($_GET['orderValue'])){
			$order = mysqli_real_escape_string($connection,$_GET['order']);
			$value =  mysqli_real_escape_string($connection,$_GET['orderValue']);
			$sql.=" ORDER BY $order $value";
		}else{
			$sql.=" ORDER BY job_title";
		}
    $result = $connection->query($sql);
    $pageCount = ceil($result->num_rows/6);
    if($page > $pageCount){
        if($pageCount != 0){
            $page = $pageCount;
        }
    }else{
        if($page < 1){
        		$page = 1;
        }
    }
    $min = ($page-1)*6;
    $sql.=" LIMIT ".$min.",6";
		$result = $connection->query($sql);
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				echo "<div class='card col-lg-10 col-xl-5 mx-auto' data-toggle='modal' data-target='#showContractorModal' name='".$row['userID']."'>
						<h5 class='card-header row  mx-0'>".$row['job_title']."<span class='ml-auto small'>".$row['state_name']."</span></h5>
						<div class='card-body row'>
							<div class='col-5 profileImage thumbnail' style='background-image:url(".$row['profile_image'].")'>
							</div>
							<div class='col-7'>
								<h5 class='card-title'>".$row['job_name']."</h5>
								<p class='card-text' max=50>".$row['description']."</p>
								<div class='row' >";
									if($row['allLikes'] != 0){
										$percentage = round($row['rating'] , 2);
                                        if($percentage <= 40) $class = 'bad';
                                        if($percentage > 40 && $percentage <= 80) $class = 'avg';
                                        if($percentage > 80) $class = 'good';
										echo "<p class='col-6 rating'>Értékelése: <span class='$class'>$percentage%</span></p>";
									}
									echo
									"
									<a href='#' class='btn col-5 btn-primary ml-auto align-self-center' style='height:100%;'>Megnézés</a>
								</div>
							</div>
						</div>
					</div>";
			}
		}else{
			echo "<div class='card col-lg-10 col-xl-5 mx-auto'>
							<h5 class='card-header row  mx-0'>Nincs találat!</h5>
							<div class='card-body'>
							<p>Sajnáljuk nem találtunk a/az '{$_GET['searchValue']}' szóra találatot.</p>
							</div>
						</div>";
		}

	?>
	<!-- Page navigation -->

	<nav aria-label="Page navigation" class="w-100">
	  <ul class="pagination justify-content-center">
		<li class="page-item <?php if($page==1) echo 'disabled';?>">
		  <a class="page-link btn" href="<?php echo $url.($page-1)?>" tabindex="-1">Előző</a>
		</li>
		<li class="page-item active">
		  <span class="page-link btn active">
			<?php echo $page?>
			<span class="sr-only">(current)</span>
		  </span>
		</li>
		<li class="page-item" style="display:<?php if($pageCount<$page+1) echo 'none';?>">
            <a class="page-link btn"
               href="
                     <?php echo $url.($page+1); ?>
                     ">
                <?php echo $page+1?>
            </a>
          </li>
		<li class="page-item" style="display:<?php if($pageCount<$page+2) echo 'none';?>">
            <a class="page-link btn"
               href="
                     <?php echo $url.($page+2); ?>
                     ">
                <?php echo $page+2?>
            </a>
          </li>
		<li class="page-item <?php if($pageCount<$page+1) echo 'disabled';?>">
            <a class="page-link btn"
               href="
                     <?php echo $url.($page+1); ?>
                     ">
                Következő
            </a>
          </li>
	  </ul>
	</nav>
</div>
</section>
<!-- show contractor info modal -->
<div class="modal fade" id="showContractorModal" tabindex="-1" role="dialog" aria-labelledby="contractorModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
            <div class="modal-header">
		<h5 class="modal-title" id="JobName">Vállalkozás adatai</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
		</button>
            </div>
            <div class="modal-body">

							<div class="form-group row">
									<div class="col-sm-4 ">
										<div class="profileImage thumbnail" id="profileImage"  style="height:155px;"></div>
										<p class="col col-form-label justify-content-center form-group row m-0">
											<a class="col-5 btn" id="like-btn"><i class="fas fa-thumbs-up"></i><span id="like-btn-display"></span></a>
											<a class="col-5 btn" id="dislike-btn"><i class="fas fa-thumbs-down"></i><span id="dislike-btn-display"></span></a>
										</p>
										<h5 id="Name" class="col"></h5>
									</div>
									<div class="col-sm-8 form-group row">
											 <h4 id="JobTitle" class="col-12"></h4>
											 <p id="JobState" class="col-12 col-form-label"></p>
											 <p id="JobDescription" class="col col-form-label"></p>
									</div>
							</div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-secondary" id="sendMessage" href="chat.php" type="button">Üzenet küldése</a>
            </div>
	</div>
    </div>
</div>
<!-- show message modal -->
<div class="modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
            <div class="modal-header">
		<h5 class="modal-title" id="JobName">Keresési hiba</h5>
		<button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
		</button>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">OK</button>
            </div>
	</div>
    </div>
</div>
</body>
<script src="js\Bootstrap\Popper.js" type="text/javascript"></script>
<script src="js\jQuery\jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js\Bootstrap\bootstrap.min.js" type="text/javascript"></script>
<script src="js/validator.js" type="text/javascript"></script>
<script src="js/search.js" type="text/javascript"></script>
</html>
