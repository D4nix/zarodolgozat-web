<?php
include 'php/connect.php';
session_start();
if(!$_SESSION['LoggedIn']){
	header("location:index.php");
}
include 'php/log.php';
logTheActivity('profile');
?>
<!DOCTYPE html>
<html>
<head lang="hu">
<meta charset="utf-8"></meta>
<title>Szaki.hu</title>
<!--bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!--general css  -->
<link rel="stylesheet" href="css/style.css">
<!--site css-->
<link rel="stylesheet" href="css/profile.css">
<!--fontawesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body>
<!--navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="loggedin.php">Szaki.hu</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-bars fa-2x" style="color:white;"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="loggedin.php">Főoldal</a>
      </li>
	    <li class="nav-item active">
        <a class="nav-link" href="search.php">Keresés</a>
      </li>
			<li class="nav-item active">
				 <a class="nav-link" href="chat.php">Társalgó</a>
			</li>

    </ul>
    <ul class="navbar-nav mr-end">
      <li class="nav-item active">
        <a class="nav-link" href="#">Profil</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="php/logout.php">Kilépés</a>
      </li>

    </ul>
  </div>
</nav>
<div  class='alert alert-danger alert-dismissible fade show'>
    <a href='#' class='close'  aria-label='close'>&times;</a>
    <strong id="alertBoxMessage">Sikertelen</strong>
</div>
<section class=" main form-section">
    <div class="contianer">
      <form id="pictureUploadForm" action="php/uploadImage.php" method="post" enctype="multipart/form-data">
						<div class="form-group row">
						<h5 class='col'>Személyes adatok</h5>
						</div>
            <div class="form-group row">
                <div class="col-sm-3 profileImage" style="background-image:url('<?php echo $_SESSION['ProfileImage']; ?>')">
										<i class="fas fa-upload"></i>
										<label for="fileToUpload"></label>
										<input type="file" name="fileToUpload" id="fileToUpload">
                    <input type="hidden" name="targetDir" value="../images/profile/">
                    <input type="hidden" name="IsProfile" value="true">
										<input id="hiddenUploadBtn" type="submit" name="submit" >
                </div>
                <div class="col-sm-9">
                    <div>
                        <label for="inputLastName" class="col-sm-3 col-form-label">Vezetéknév </label>
                        <input type="text" class="form-control" id="inputLastName" name="last_name" placeholder="Vezetéknév" onfocus="testName('inputLastName')" onfocusout="update('inputLastName','users'),dismissPopover('inputLastName')" onkeyup="testName('inputLastName')"
                                value="<?php if(isset($_SESSION['LastName'])) echo $_SESSION['LastName']; ?>" data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
                                data-content=
                                "<ul>
                                  <li>Nagybetűvel kell kezdödnie</li>
                                  <li>Csak betűt tartalmazhat</li>
                                  <li>Nem tartalmazhat számot</li>
                          </ul>"
                          >
                        <span class="form-icon" id="inputLastNameIcon"><i class="fas fa-exclamation"></i></span>
                    </div>
                    <div>
                        <label for="inputFirstName" class="col-sm-3 col-form-label">Keresztnév </label>
                        <input type="text" class="form-control" id="inputFirstName" name="first_name" placeholder="Keresztnév" onfocus="testName('inputFirstName')" onfocusout="update('inputFirstName','users'),dismissPopover('inputFirstName')" onkeyup="testName('inputFirstName')"
                               value="<?php if(isset($_SESSION['FirstName'])) echo $_SESSION['FirstName']; ?>"
                               data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
                               data-content=
																	"<ul>
													          <li>Nagybetűvel kell kezdödnie</li>
																		<li>Csak betűt tartalmazhat</li>
																		<li>Nem tartalmazhat számot</li>
																	</ul>"
                        			 >
												<span class="form-icon" id="inputFirstNameIcon"><i class="fas fa-exclamation"></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEmail" class="col-sm-3 col-form-label">Email </label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email"  onfocus="testEmail('inputEmail')" onfocusout="update('inputEmail','users'),dismissPopover('inputEmail')" onkeyup="testEmail('inputEmail')"
										   value="<?php if(isset($_SESSION['Email'])) echo $_SESSION['Email']; ?>"
										   data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
										   data-content=
													  "<ul>
														  <li>Tartalmaznia kell '@' karaktert</li>
														  <li>Tartalmaznia kell '.' karaktert</li>
													  </ul>"
				            >
										<span class="form-icon" id="inputEmailIcon"><i class="fas fa-exclamation"></i></span>
							  </div>
			      </div>
            <div class="form-group row">
							<label for="inputPassword" class="col-sm-3 col-form-label">Jelszó </label>
							<div class="col-sm-9 ">
								<button type="button" class="btn btn-secondary " data-toggle="modal" data-target="#changePasswordModal">Jelszó megvátoztatása</button>
							</div>
						</div>
			<?php
				if($_SESSION['category'] == 'contractor'){
					$sql = 'SELECT job_name,job_title,state,description,
                            COALESCE(COUNT(likes.userID),0) AS "allLikes" ,
                            COALESCE(SUM(likes.liked),0) AS "likes",
                            COALESCE(SUM(likes.liked),0)  / COALESCE(COUNT(likes.contractorID),0)*100 AS "rating"
                            FROM contractors INNER JOIN likes ON  contractors.userID = likes.contractorID
                            WHERE contractors.userID='.$_SESSION["UserID"];
					$result = $connection->query($sql);
					$row = $result->fetch_assoc();
					echo '<div class="form-group row">';
					echo '<h5 class="col">Válalkozás adatai</h5>';
					echo '</div>';

					echo '<div class="form-group row">';
					echo ' <label for="inputJobName" class="col-sm-3 col-form-label">Válalkozás neve(nem kötelező)</label>
						   <div class="col-sm-9">
								<input type="text" class="form-control" id="inputJobName" name="job_name" placeholder="Cég név" onfocusout="update(\'inputJobName\',\'contractors\')"
										value="';
							if(!empty($row["job_name"])) echo $row["job_name"];
							echo '"></div>';
					echo '</div>';

					echo '<div class="form-group row">';
					echo ' <label for="inputJobTitle" class="col-sm-3 col-form-label">Szakma megnevezése</label>
						   <div class="col-sm-9">
								<input type="text" class="form-control" id="inputJobTitle" name="job_title" placeholder="Szakma megnevezése" onfocusout="update(\'inputJobTitle\',\'contractors\')"
										value="';
							if(!empty($row["job_title"])) echo $row["job_title"];
							echo '"></div>';
					echo '</div>';

					echo '<div class="form-group row">';
					echo ' <label for="selectJobState" class="col-sm-3 col-form-label">Válalkozás székhelye(megye)</label>
						   <div class="col-sm-9">
								<select class="form-control" id="selectJobState" name="selectJobState">
                                <option value="null">Válasszon megyét!</option>';
								$result = $connection->query('SELECT id, state_name FROM states');
								while($state = $result->fetch_assoc()){
									$own = '' ;
									if($state['id'] == $row['state']) $own = 'selected';
									echo "<option value='{$state['id']}' $own>{$state['state_name']}</option>";
								}

					echo '</select>
								</div>';
					echo '</div>';

					echo '<div class="form-group row">';
					echo ' <label for="inputJobDescription " class="col-sm-3 col-form-label">Válalkozás leírása</label>
						   <div class="col-sm-9">
								<textarea type="text" class="form-control" id="inputJobDescription" name="description" placeholder="Válalkozás leírása"  onblur="update(\'inputJobDescription\',\'contractors\')"
									max="100">';
							if(!empty($row["description"])) echo $row["description"];
							echo '</textarea></div>';
					echo '</div>';

					echo '<div class="form-group row">';
                    if($row['allLikes'] != 0){
										$percentage = round($row['rating'] , 2);
                                        if($percentage <= 40) $class = 'bad';
                                        if($percentage > 40 && $percentage <= 80) $class = 'avg';
                                        if($percentage > 80) $class = 'good';
                                        $dislikes =$row['allLikes']-$row['likes'];
                                        echo " <label class='col-sm-3 col-form-label'>Értékelése:</label>
						                          <div class='col-sm-9' style='padding-top: calc(.375rem + 1px);padding-bottom: calc(.375rem + 1px);'>
									                   <p>
                                                            <span class='$class'>$percentage%</span>
                                                            <span>Értékelések száma:{$row['allLikes']}</span>
                                                            <span>
                                                                <i class='far fa-thumbs-up'></i>
                                                                :{$row['likes']}
                                                            </span>
                                                            <span>
                                                                <i class='far fa-thumbs-down'></i>
                                                                :$dislikes
                                                            </span>
                                                        </p>
							                      </div>";
                    }else{
                        echo ' <label class="col-sm-3 col-form-label">Értékelése:</label>
						                          <div class="col-sm-9" style="padding-top: calc(.375rem + 1px);padding-bottom: calc(.375rem + 1px);">
									                   <p>
                                                            Jelenleg még nincs értékelve.
                                                        </p>
							                      </div>';
                    }

					echo '</div>';
				}
			?>
      </form>
    </div>

</section>
<!-- Change password modal -->
<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
			<div class="modal-content">
            <div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Jelszó megváltoztatása:</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
							</button>
            </div>
            <div class="modal-body">
						 <form>
							<div class="form-group row">
								<label for="inputPasswordNow" class="col-sm-3 col-form-label">Jelenlegi jelszó </label>
								<div class="col-sm-8 ">
									<input type="password" class="form-control" id="inputPasswordNow" name="inputPasswordNow" placeholder="Jelszó" onfocus="testPassword('inputPasswordNow')" onfocusout="testPassword('inputPasswordNow'),dismissPopover('inputPasswordNow')" onkeyup="testPassword('inputPasswordNow')"
									data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
									data-content=
												"<ul>
													<li>Minimum 9  karakter hoszúnak kell lennie </li>
													<li>Tartalmaznia kell speciális karaktert</li>
													<li>Tartalmaznia kell kis- és nyagybetüt</li>
													<li>Tartalmaznia kell számot</li>
												</ul>"
									>
									<span class="form-icon" id="inputPasswordNowIcon"><i class="fas fa-exclamation"></i></span>
								</div>
							</div>
                <hr style='background-color:#de1a1a;border-width:0;color:#000000;height:2px;line-height:0;text-align:left;width:100%;'>
								<div class="form-group row">
									<label for="inputPassword" class="col-sm-3 col-form-label">Jelszó </label>
									<div class="col-sm-8 ">
										<input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Jelszó" onfocus="testPassword('inputPassword')" onfocusout="testPassword('inputPassword'),dismissPopover('inputPassword')" onkeyup="testPassword('inputPassword')"
										data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
										data-content=
													"<ul>
														<li>Minimum 9  karakter hoszúnak kell lennie </li>
														<li>Tartalmaznia kell speciális karaktert</li>
														<li>Tartalmaznia kell kis- és nyagybetüt</li>
														<li>Tartalmaznia kell számot</li>
													</ul>"
										>
										<span class="form-icon" id="inputPasswordIcon"><i class="fas fa-exclamation"></i></span>
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPasswordAgain" class="col-sm-3 col-form-label">Jelszó újra </label>
									<div class="col-sm-8">
										<input type="password" class="form-control" id="inputPasswordAgain" placeholder="Jelszó mégegyszer" onfocus="testPassword('inputPasswordAgain')" onfocusout="testPassword('inputPasswordAgain'),dismissPopover('inputPasswordAgain')" onkeyup="testPassword('inputPasswordAgain')"
										data-toggle="popover" data-trigger="manual" data-html="true" title="Követelmények"
										data-content=
													"<ul>
														<li>Minimum 9  karakter hoszúnak kell lennie </li>
														<li>Tartalmaznia kell speciális karaktert</li>
														<li>Tartalmaznia kell kis- és nyagybetüt</li>
														<li>Két jelszónak meg kell egyezniük</li>
														<li>Tartalmaznia kell számot</li>
													</ul>"
										>
										<span class="form-icon" id="inputPasswordAgainIcon"><i class="fas fa-exclamation"></i></span>
									</div>
						 </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" id ="cancelBtn" type="button" data-dismiss="modal">Cancel</button>
				<button class="btn btn-primary" onclick="changePassword()" >Save</button>
            </div>
					</div>
    	</div>
		</div>
</div>
<!--jquery -->
<script src="js\Bootstrap\Popper.js" type="text/javascript"></script>
<script src="js\jQuery\jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js\Bootstrap\bootstrap.min.js" type="text/javascript"></script>
<script src="js/validator.js" type="text/javascript"></script>
<script src="js/profile.js" type="text/javascript"></script>
</html>
