function testName(idName){
	var value = document.getElementById(idName).value;
	var valid = false;
	if(value!=""){
		valid = validateName(value);
	}
	if(!valid){
		$("#"+idName).popover("show");
		$("#"+idName+"Icon").show();
		return false;
	}else{
		$("#"+idName).popover("hide");
		$("#"+idName+"Icon").hide();
		return true;
	}
}

function testEmail(idName){
	var value = document.getElementById(idName).value;
	var valid = false;
	if(value!=""){
		valid = validateEmail(value);
	}
	if(!valid){
		$("#"+idName).popover("show");
		$("#"+idName+"Icon").show();
		return false;
	}else{
		$("#"+idName).popover("hide");
		$("#"+idName+"Icon").hide();
		return true;
	}
}
function testPassword(idName){
	var value = document.getElementById(idName).value;
	var valid = false;
	if(value!=""){
		valid = validatePassword(value);
	}
	if(!valid){
		$("#"+idName).popover("show");
		$("#"+idName+"Icon").show();
		return false;
	}else{
		$("#"+idName).popover("hide");
		$("#"+idName+"Icon").hide();
		return true;
	}
}

function validateName(name){
	name = name.trim();
	var pat = /(^[A-ZÖÜÓŐÚÉÁŰ])+[a-zöüóőúéáű]+$/;
	if(pat.test(name) && name.length >=3){
		return true;
	}else{
		return false;
	}
}
function validateEmail(email){
	var pat = /^[a-zöüóőúéáűA-ZÖÜÓŐÚÉÁŰ0-9._-]+@[a-zA-ZÖÜÓŐÚÉÁŰ0-9.-]+\.[a-zöüóőúéáűA-ZÖÜÓŐÚÉÁŰ]{2,6}$/;
	if(pat.test(email)){
		return true;
	}else{
		return false;
	}
}
function validatePassword(password){
	var pat = /^(?=.*[A-ZÖÜÓŐÚÉÁŰ])/;
	var pat2 = /^(?=.*[0-9])/;
	if(pat.test(password) && pat2.test(password) && password.length >=9){
		return true;
	}else{
		return false;
	}
}
