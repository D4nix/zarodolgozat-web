$(document).ready(function(){
  ChangeSize();
  let isLiked = false;
  // kártyákra kattintás függvény
  $(document).on("click", ".card", function () {
   let id = $(this).attr('name');
   $("#like-btn").removeClass("disabled");
   $("#dislike-btn").removeClass("disabled");

   let sql = "SELECT users.userID,first_name,last_name,job_name,job_title, description, "+
             "COALESCE(state_name,'') AS 'state_name', profile_image, "+
             "COALESCE(COUNT(likes.contractorID),0) AS 'allLikes' , "+
             "COALESCE(SUM(likes.liked),0) AS 'likes' "+
             "FROM contractors INNER JOIN users ON contractors.userID = users.userID LEFT JOIN states ON contractors.state = states.id LEFT JOIN likes ON contractors.userID = likes.contractorID "+
             "GROUP BY users.userID "+
             "HAVING users.userID ="+id;
   $.ajax({
       type: 'POST',
       url:"php/sqlfunctions.php",
       dataType: 'json',
       data: {
         sql : 'selectOne',
         sqlLines : sql
         },
       success:function(contractor){
          $("#Name").html(contractor.first_name + " " + contractor.last_name);
          $("#JobName").html(contractor.job_name);
          $("#profileImage").css('background-image','url('+contractor.profile_image+')');

          let userID = $('body').attr('data');
          sql = "SELECT id,liked FROM likes WHERE contractorID = "+contractor.userID+" AND userID = "+userID;
          $.ajax({
              type: 'POST',
              url:"php/sqlfunctions.php",
              dataType: 'json',
              data: {
                sql : 'selectOne',
                sqlLines : sql
                },
              success:function(liked){
                    if(liked.liked == 1){
                        $("#like-btn").addClass("disabled");
                        isLiked = true;
                    }else{
                        $("#dislike-btn").addClass("disabled");
                        isLiked = true;
                    }
              }
          });
          $("#like-btn-display").html(" "+contractor.likes);
          $("#dislike-btn-display").html(" "+contractor.allLikes-contractor.likes);
          $("#like-btn").attr('name',contractor.userID);
          $("#dislike-btn").attr('name',contractor.userID);

          $("#JobTitle").html(contractor.job_title);
          $("#JobState").html(contractor.state_name);
          $("#JobDescription").html(contractor.description);
          $("#sendMessage").attr('href','chat.php?user='+contractor.userID);

       }
   });
  });

  // Likeolás
  $('#like-btn').click(function(){
      let id = $(this).attr('name');
      $.ajax({
          type: 'POST',
          url:"php/like.php",
          data: {
            id : id,
            rateValue : 1
          },
          success:function(){

          }
      });
      $('#like-btn').addClass("disabled");
      $("#dislike-btn").removeClass("disabled");

      let dislike = parseInt($("#dislike-btn-display").html());
      let like = parseInt($("#like-btn-display").html())
      like+=1;

      $("#like-btn-display").html(like);
      if(isLiked){
        dislike-=1
        $("#dislike-btn-display").html(dislike);
      }

      let percentage = like/(dislike+like)*100;
      if(percentage % 1 === 0){
          percentage = percentage.toFixed();
      } else{
          percentage = percentage.toFixed(2);
      }
      $('[name="'+id+'"]').find(".rating").children().html(percentage+"%");
  });
  // Dislikeolás
  $('#dislike-btn').click(function(){
      let id = $(this).attr('name');
      $.ajax({
          type: 'POST',
          url:"php/like.php",
          data: {
            id : id,
            rateValue : 0
          },
          success:function(){

          }
      });
      $('#dislike-btn').addClass("disabled");
      $("#like-btn").removeClass("disabled");

      let dislike = parseInt($("#dislike-btn-display").html());
      let like = parseInt($("#like-btn-display").html())
      dislike+=1;

      $("#dislike-btn-display").html(dislike);
      if(isLiked){
        like-=1;
        $("#like-btn-display").html(like);
      }

      let percentage = like/(dislike+like)*100;
      if(percentage % 1 === 0){
          percentage = percentage.toFixed();
      } else{
          percentage = percentage.toFixed(2);
      }
      $('[name="'+id+'"]').find(".rating").children().html(percentage+"%");
  });
});

//Set the height of the main div
function ChangeSize() {
  let hg = $(".navbar").css("height");
  $(".main").css("margin-top",hg);
}

//change main div height on resize
$( window ).resize(function() {
    ChangeSize();
});

$(function () {
  $('[data-toggle="popover"]').popover()
})

$('#searchBtn').click(function(){
  let text = $('#searchValue').val();
  $(location).attr('href','search.php?search=job_title&searchValue='+text+'&order=job_title&orderValue=ASC');
});
