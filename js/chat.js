//change main div height on resize
$( window ).resize(function() {
		ChangeSize();
});

//document ready function
$(document).ready(function(){
		ChangeSize();
    getMessage();
		var interval = setInterval(function(){
					getMessage();
		},10000);
});

///Show the online div
$("#onlineUserToggleBtn").click(function(){
		$("#onlineDiv").show();
		$("#onlineDiv").css("position","absolute");
		$("#onlineDiv").css("left","0");
		$("#background").show();
		$("#onlineDiv").css("height",'100%');
});

//Close online div if the user clicked on the background
$("#background").click(function(){
		CloseOnlineDiv();
		CloseBigImage();
});

//Send message
$('[name="taker"').click(function(){
    let PageURL = window.location.search.substring(1);
    //let taker = PageURL.split('=');
		let taker = $(this).val();
		console.log(taker);
    let message = $('[name="message"]').val();
		$.ajax({
        type: 'POST',
        url:"php/sendMessage.php",
        data: {
          message : message,
          taker : taker
          },
        success:function(message){
						$('[name="message"]').val("");
            getMessage()
        }
    });
});

//Close the online div
function CloseOnlineDiv(){
		$("#onlineDiv").hide();
		$("#onlineDiv").css("position","absolute");
		$("#onlineDiv").css("left","none");
		$("#background").hide();
}

//Close the online div
function CloseBigImage(){
        $("#bigImageDiv").fadeOut();
		//$("#bigImageDiv").css("display","none");
        $("#background").appendTo('.main');
		$("#background").css({
		"display":"none",
		'z-index':'10'
		});

}

//Set the height of the main div
function ChangeSize() {
		let hg = $(".navbar").css("height");
		$(".main").css("margin-top",hg);
		//if($(window).height() > 700){
		hg = $(window).height()- hg.replace("px","");
		$(".main").css("height",hg);
		if(parseInt($(window).width()) < '992'){
			if($("#onlineDiv").hasClass("col-3")){
				$("#onlineDiv").toggleClass('col-3 col-6');
			}


		}else {
			if($("#onlineDiv").hasClass("col-6")){
				$("#onlineDiv").toggleClass('col-6 col-3');
			}
			CloseOnlineDiv();
		}
}

//Get message from database
var messages;
function getMessage() {
  let PageURL = window.location.search.substring(1);
  let taker = PageURL.split('=');
  let sender = $('body').attr('data');
	if(taker[1]!='false'){
    $.ajax({
	       type: 'POST',
	       url:"php/getMessage.php",
	       data: {
           taker : taker[1],
	         sender : sender
	        },
	       	success:function(NewMessages){
						if(messages!=NewMessages && NewMessages!=""){
							messages=NewMessages;
							$('#messagesDiv').html(messages);
							$("#messagesDiv").animate({ scrollTop: $("#messagesDiv").prop("scrollHeight")}, 1000);

						}
	        }
	  });
	}
}

$(function () {
	$('[data-toggle="popover"]').popover()
})


//Upload image from chat
$("#uploadImageInput").change(function(){
    var formData = new FormData();
    //console.log($('#uploadImageInput')[0].files[0]);
    formData.append('fileToUpload', $('#uploadImageInput')[0].files[0]);
    formData.append('targetDir','../images/chatImages/');
    formData.append('IsProfile','false');
    let PageURL = window.location.search.substring(1);
    let taker = PageURL.split('=');
    formData.append('taker',taker[1]);
    $.ajax({
           url : 'php/uploadImage.php',
           type : 'POST',
           data : formData,
           processData: false,  // tell jQuery not to process the data
           contentType: false,  // tell jQuery not to set contentType
           success : function(data) {
               getMessage();
           }
    });
});

var imagesArray;
//Show chat image in big size
function showImage(src,thisElement){

  $("#background").appendTo('#bigImageDiv');

  let elementPosition = $(thisElement).offset();
	let elementHeight = $(thisElement).height();
	let elementWidth = $(thisElement).width();

	let PageURL = window.location.search.substring(1);
	let taker = PageURL.split('=');

	$.ajax({
		url:'php/getImages.php',
		type: 'POST',
		dataType: 'json',
		data: {
			current : src.message,
			taker:taker[1]
			},
			success:function(respond) {
				imagesArray = respond;
				//console.log(imagesArray);
				$('#bigImage').attr('src',imagesArray.images[imagesArray.id].message);
				$('#bigImageInsideDiv').css({
            'height':elementHeight,
						'width':elementWidth,
						'top':(elementPosition.top),
						'left':(elementPosition.left),
            'position':'absolute',

				});
        $("#bigImageInsideDiv").animate({
            top: $(window).height()/2-(($(window).height()*0.8)/2)+'px',
            left:$(window).width()/2-(($(window).width()*0.7)/2)+'px',
            height:'80vh',
            width:'70vw'
        },1000);
				$("#background").css({
				"display":"block",
				'z-index':'80'
				});
        $('#bigImageDiv').css('display','block');
			},
	});
}

//Show previous picture
function previousPicture() {
	if(imagesArray.id !=0){
		imagesArray.id--;
		LoadPicture();
	}
}

//Show next picture
function nextPicture() {
	if(imagesArray.images.length-1 != imagesArray.id){
		imagesArray.id++;
		LoadPicture();
	}
}

function LoadPicture() {
	$('#bigImage').attr('src',imagesArray.images[imagesArray.id].message);
}
