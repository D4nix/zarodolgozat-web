function runTDD(){
  TDDFirstNameUppercaseFirstLetterTrue();
  TDDFirstNameUppercaseFirstLetterFalse();
  TDDFirstNameShort();
}

function TDDFirstNameUppercaseFirstLetterTrue(){
  var actual = validateName("Dániel");
  if(actual){
    console.log("TDDFirstNameUppercaseFirstLetterTrue:OK");
  }else{
    console.log("TDDFirstNameUppercaseFirstLetterTrue:Hibás");
  }
}
function TDDFirstNameUppercaseFirstLetterFalse(){
  var actual = validateName("daniel");
  if(!actual){
    console.log("TDDFirstNameUppercaseFirstLetterFalse:OK");
  }else{
    console.log("TDDFirstNameUppercaseFirstLetterFalse:Hibás");
  }
}
function TDDFirstNameShort(){
  var actual = validateName("Da");
  if(!actual){
    console.log("TDDFirstNameShort:OK");
  }else{
    console.log("TDDFirstNameShort:Hibás");
  }
}
