var OriginalValue;
$(document).ready(function(){
	   ChangeSize();

	   $("select").change(function(){
		   let val = $(this).val();
		   $.ajax({
	 				 type: 'POST',
	 				 url:"php/sqlfunctions.php",
	 				 data: {
						 sql : 'update',
						 table : 'contractors',
						 whichData:'state',
						 data  : val
						 },
	 				 success:function(data){
						ShowMessageBox(data,'70%','green');
	 				 }
	 		});
		});

		$(".form-control").click(function(){
				OriginalValue = $(this).val();
		});
});

//Set the height of the main div
function ChangeSize() {
	let hg = $(".navbar").css("height");
	$(".main").css("margin-top",hg);
}

//change main div height on resize
$( window ).resize(function() {
		ChangeSize();
});

$('#fileToUpload').change(function() {
	console.log("asd");
	$('#hiddenUploadBtn').trigger( "click" );
});
function update(dataName,table){
	let data = $('#'+dataName).val();
	let whichData = $('#'+dataName).attr('name');
	let valid = false;
	switch(whichData){
		case "last_name":
			if(testName('inputLastName')){
				valid = true;
			};break;
			case "first_name":
				if(testName('inputFirstName')){
					valid = true;
				};break;
				case "email":
					if(testEmail('inputEmail')){
						valid = true;
					};break;
		default:valid = true;break;
	}
	if(valid){
		if(OriginalValue != data){
			$.ajax({
		 				 type: 'POST',
		 				 url:"php/sqlfunctions.php",
		 				 data: {
							 sql : 'update',
							 table : table,
							 whichData:whichData,
							 data  : data
							 },
		 				 success:function(respond){
							if(respond.includes("Sikertelen")){
								ShowMessageBox(respond,'70%','#de1a1a');
								$('#'+dataName).val(OriginalValue+"");
							}else{
								ShowMessageBox(respond,'70%','green');
							}
		 				 }
		 	});
		}
	}
}

//change password
function changePassword(){
    let inputPasswordNow = $('#inputPasswordNow').val();
    let inputPassword = $('#inputPassword').val();
    let inputPasswordAgain = $('#inputPasswordAgain').val();
    let valid = true;
    if(inputPassword == inputPasswordAgain){
        if(!validatePassword(inputPassword)) valid = false;
        if(!validatePassword(inputPasswordAgain)) valid = false;
        if(inputPasswordNow.trim()!="" && valid){
            valid = validatePassword(inputPasswordNow);
            if(valid){
                $.ajax({
                             type: 'POST',
                             url:"php/changePassword.php",
                             data: {
                                 checkPassword : true,
                                 password:inputPasswordNow
                             },
                             success:function(respond){
                                    if(respond=='true'){
                                        $.ajax({
                                                type: 'POST',
                                                url:"php/changePassword.php",
                                                data: {
                                                    password:inputPassword
                                                },
                                                success:function(respond){
                                                    if(respond=='true'){
																												HideMessageBox();
                                                        $('#changePasswordModal').modal('hide');
                                                    }else{
																												ShowMessageBox('Sikertelen jelszó frisítés.',$('.modal-dialog').width(),'#de1a1a');
                                                    }
                                                }
                                        });
                                    }else{
																				ShowMessageBox('A jelszó nem egyezik a jelenlegi jelszóval!',$('.modal-dialog').width(),'#de1a1a');
                                    }
                             }
                        });
            }
        }
    }else{
			 ShowMessageBox('A két jelszó nem egyezik!',$('.modal-dialog').width(),'#de1a1a');
    }
}

$('.close').click(function() {
	HideMessageBox();
});

function ShowMessageBox(message,width,backgroundColor) {
	$('#alertBoxMessage').text(message);
	$(".alert").css('width',width);
	$(".alert").css('background-color',backgroundColor);
	$(".alert").animate({opacity: "1"});
}

function HideMessageBox(backgroundColor) {
	$(".alert").animate({opacity: "0"},250);
}

function dismissPopover(id){
	$("#"+id).popover("hide");
}

$(function () {
		$('[data-toggle="popover"]').popover()
});
$('#pictureUploadForm').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) {
    e.preventDefault();
    return false;
  }
});
