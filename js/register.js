$(document).ready(function(){
  ChangeSize();
  runTDD();
});

//Set the height of the main div
function ChangeSize() {
  let hg = $(".navbar").css("height");
  $(".main").css("margin-top",hg);
}

//change main div height on resize
$( window ).resize(function() {
    ChangeSize();
});

$(function () {
  $('[data-toggle="popover"]').popover()
})

function dismissPopover(id){
  $("#"+id).popover("hide");
}

var validInputs = {
  inputLastName:      false,
  inputFirstName:     false,
  inputEmail:         false,
  inputPassword:      false,
  inputPasswordAgain: false
};
function disabledRegisterButton(){
  var disable = false;
  for(var i = 0; i<5 ; i++){
    if(validInputs[i] == false){
      disable = true;
    }
  }
  if(disable){
    document.getElementById('registerButton').disabled = true;
  }else{
    if($('#inputPassword').val() == $('#inputPasswordAgain').val()){
      document.getElementById('registerButton').disabled = false;
    }else{
      document.getElementById('registerButton').disabled = true;
      $('#inputPasswordAgain').popover("show");
      $('#inputPasswordAgain').show();
    }
  }
}
function checkName(idName,idNumber){
  valid = testName(idName);
  if(valid){
    validInputs[idNumber] = true;
    disabledRegisterButton();
  }else {
    validInputs[idNumber] = false;
    disabledRegisterButton();
  }
}
function checkEmail(idName,idNumber){
  valid = testEmail(idName);
  if(valid){
    validInputs[idNumber] = true;
    disabledRegisterButton();
  }else{
    validInputs[idNumber] = false;
    disabledRegisterButton();
  }
}

function checkPassword(idName,idNumber){
  var value = document.getElementById(idName).value;
  var valid = false;
  if(value!=""){
    valid = validatePassword(value);
  }
  if(!valid){
    $("#"+idName).popover("show");
    $("#"+idName+"Icon").show();
    validInputs[idNumber] = false;
    disabledRegisterButton();
  }else{
    $("#"+idName).popover("hide");
    $("#"+idName+"Icon").hide();
    validInputs[idNumber] = true;
    disabledRegisterButton();
  }
}

function change(action){
  document.getElementById("divJobTitle").style.display=action;
}

$(function () {
  $('[data-toggle="popover"]').popover()
})
