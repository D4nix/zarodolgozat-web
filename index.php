<?php
include 'php/connect.php';
session_start();
if(isset($_SESSION['LoggedIn'])){
	header("location:loggedin.php");
}
?>
<!DOCTYPE html>
<html>
<head lang="hu">
<meta charset="utf-8"></meta>
<title>Szaki.hu</title>
<!--bootstrap -->
<link rel="stylesheet" href="css\Bootstrap\bootstrap.min.css">
<!-- general css -->
<link rel="stylesheet" href="css/style.css">
<!--site css-->
<link rel="stylesheet" href="css/index.css">
<!--fontawesome -->
<link rel="stylesheet" href="css\FontAwesome\css\all.css">
</head>
<body>
<!--navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Szaki.hu</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-bars fa-2x" style="color:white;"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Főoldal</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="register.php">Regisztráció</a>
      </li>
    </ul>
  </div>
</nav>
<section id="welcome" class=" main row justify-content-center">
	<h2 class=" col-sm-8 align-self-center section-text">Szakembert keres,akkor jó helyen jár mert a szaki.hu-n megtalálja</h2>
</section>
<section class="row" style="padding:0!important;margin:0!important;">
<div class=" col-lg-6" id="loginImage" style="height:50vh;">

</div>
<div class="  col-lg-6" style="height:50vh; ">
	<?php
		if(isset($_GET['message']) AND !empty($_GET['message'])){
			echo
			"<div  class='alert alert-danger alert-dismissible fade show'>
    			<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    			<strong>Helytelen bejelentkezési adatok!</strong>
					{$_GET['message']}
  		</div>";
		}

	 ?>
	<form action="php/login.php" method="POST">
	  <div class="form-group row justify-content-center" style="margin-top:1vh">
		<i class="far fa-user fa-10x  "></i>
	  </div>
	  <div class="form-group row">
		<label for="inputEmail" class="col-sm-3 col-form-label">Email </label>
		<div class="col-sm-9">
		  <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email" value="<?php if(isset($_SESSION['Email'])) echo $_SESSION['Email']; ?>">
		</div>
	  </div>
	  <div class="form-group row">
		<label for="inputPassword" class="col-sm-3 col-form-label">Jelszó </label>
		<div class="col-sm-9">
		  <input type="password" class="form-control" id="inputPassword"  name="inputPassword" placeholder="Jelszó">
		</div>
	  </div>


	  <div class="form-group row justify-content-center">
		<div class="col-sm-3">
		  <button type="submit" name="login" class="btn btn-secondary">Belépés</button>
		</div>
	  </div>
	</form>
</div>
</section>
</body>
<!--jquery -->
<script src="js\Bootstrap\Popper.js" type="text/javascript"></script>
<script src="js\jQuery\jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js\Bootstrap\bootstrap.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	ChangeSize()
});
//Set the height of the main div
function ChangeSize() {
	let hg = $(".navbar").css("height");
	$(".main").css("margin-top",hg);
}

//change main div height on resize
$( window ).resize(function() {
		ChangeSize();
});
</script>
</html>
