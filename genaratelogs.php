<?php
include 'php/connect.php';
session_start();
if(!$_SESSION['LoggedIn']){
	header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head lang="hu">
<meta charset="utf-8"></meta>
<title>Szaki.hu</title>
<!--bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!--saját -->
<link rel="stylesheet" href="css/style.css">
<!--fontawesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body>
<!--navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Szaki.hu</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-bars fa-2x" style="color:white;"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Főoldal</a>
      </li>

    </ul>
  </div>
</nav>
<section class="main">
	<input type="number" id="numberOf" value="1" min="0" max="50">
	<button type="submit" class="btn btn-secondary">Genarál</button>

</section>
</body>
<!--jquery -->
<script src="js\Bootstrap\Popper.js" type="text/javascript"></script>
<script src="js\jQuery\jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js\Bootstrap\bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery-dateformat.min.js" type="text/javascript"></script>
<script src="js/genaratelogs.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
		let hg = $(".navbar").css("height");
		$(".main").css("margin-top",hg);
	});
	$(function () {
		$('[data-toggle="popover"]').popover()
	})
</script>
</html>
