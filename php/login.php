<?php
require 'connect.php';
session_start();
if(isset($_POST['login']) && isset($_POST['inputEmail']) && isset($_POST['inputPassword'])){
	$_SESSION['Email']    = mysqli_real_escape_string($connection,$_POST['inputEmail']);
	$password = $_POST['inputPassword'];
	$query  = "SELECT * FROM users WHERE email ='".$_SESSION['Email']."'";
	$result = $connection->query($query);
	if($result->num_rows > 0){
		$row = $result->fetch_assoc();
		if(password_verify($password,$row['password'])){
			$_SESSION['UserID'] = $row['userID'];
			$_SESSION['category'] = $row['category'];
			$_SESSION['LastName'] = $row['last_name'];
			$_SESSION['FirstName'] = $row['first_name'];
      $_SESSION['ProfileImage'] = $row['profile_image'];
			$_SESSION['LoggedIn'] = true;
			header("location:../loggedin.php");
			die();
		}
		header("location:../index.php?message=Helytelen a jelszó!");
	}else{
		header("location:../index.php?message=Helytelen e-mail cím!");
	}
}else{
	header("location:../index.php");
}
