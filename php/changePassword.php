<?php
require 'connect.php';
session_start();
if(isset($_POST['checkPassword']) AND isset($_POST['password'])){
  $password = mysqli_real_escape_string($connection,$_POST['password']);
  $sql = "SELECT password FROM users WHERE userID=".$_SESSION['UserID'];
  $result = $connection->query($sql);
  if($result->num_rows == 1){
    $row = $result->fetch_assoc();
    if(password_verify($password,$row['password'])){
      echo 'true';
      die();
    }else {
      echo 'false';
      die();
    }
  }
  echo 'false';
  die();
}else{
  if(isset($_POST['password'])){
      $password = password_hash(mysqli_real_escape_string($connection,$_POST['password']),PASSWORD_BCRYPT);
      $sql = "UPDATE users SET password='$password' WHERE userID=".$_SESSION['UserID'];
      $connection->query($sql);
      echo 'true';
      die();
  }
}
