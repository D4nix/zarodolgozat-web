<?php
require 'connect.php';
session_start();
if (isset($_POST['searchBtn'])) {
  //header location
  $location = "location:../search.php";
  //if the location edited than true
  $edited = false;
  //seach for job title
  if(isset($_POST['inputSearchJobTitle']) AND !empty($_POST['inputSearchJobTitle']))
  {
    $title = mysqli_real_escape_string($connection,$_POST['inputSearchJobTitle']);
    if(!$edited){
      $location .= "?";
      $edited = true;
    }
    $location .= "search=job_title&searchValue=$title";
  }
  //order the contractor
  if(isset($_POST['order']) AND !empty($_POST['order']))
  {
    $orderthis = mysqli_real_escape_string($connection,$_POST['order']);
	if($_POST['checkboxOrder']){
		$value="DESC";
	}else{
		$value="ASC";
	}
    if(!$edited){
      $location .= "?";
      $edited = true;
    }else{
	  $location .= "&";
	}
    $location .= "order=$orderthis&orderValue=$value";
  }
  header($location);
}else{
  header("location:../search.php");
  die();
}

 ?>
