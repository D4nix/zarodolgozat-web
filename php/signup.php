<?php
require 'connect.php';
session_start();
if(isset($_POST['category']) && isset($_POST['inputLastName']) && isset($_POST['inputFirstName']) && isset($_POST['inputEmail']) && isset($_POST['inputPassword'])){
	$_SESSION['category']      = mysqli_real_escape_string($connection,$_POST['category']);
	$_SESSION['LastName'] = mysqli_real_escape_string($connection,$_POST['inputLastName']);
	$_SESSION['FirstName'] = mysqli_real_escape_string($connection,$_POST['inputFirstName']);
	$_SESSION['Email']    = mysqli_real_escape_string($connection,$_POST['inputEmail']);
	$_SESSION['JobTitle']    = mysqli_real_escape_string($connection,$_POST['inputJobTitle']);
  $_SESSION['ProfileImage'] = "images/Profile.svg";
	$password = password_hash(mysqli_real_escape_string($connection,$_POST['inputPassword']),PASSWORD_BCRYPT);

	$queryTest  = "SELECT email FROM users WHERE email ='".$_SESSION['Email']."'";
	$result = $connection->query($queryTest);
	if($result->num_rows == 0){
                $sql= "INSERT INTO users (first_name, last_name, email, password, category) VALUES ('".$_SESSION['LastName']."','".$_SESSION['FirstName']."','".$_SESSION['Email']."','".$password."','".$_SESSION['category'] ."');".
                        "SELECT userID FROM users WHERE email ='".$_SESSION['Email']."'";
                $connection->multi_query($sql);
                do {
                    if ($res = $connection->store_result()) {
	                    $row = $res->fetch_all(MYSQLI_ASSOC);
											$_SESSION['UserID'] = $row[0]['userID'];
											if($_SESSION['category'] == 'contractor'){
												$query2 = "INSERT INTO contractors (userID, job_title) VALUES (".$row[0]['userID'].",'".$_SESSION['JobTitle']."');";
												$connection->query($query2);
											}
	                    $res->free();
                    }
                } while ($connection->more_results() && $connection->next_result());
									$_SESSION['LoggedIn'] = true;
									header("location:../loggedin.php");
									die();
	}else{
		header("location:../register.php?message=Evvel az e-mail címmel már regisztráltak!");
		die();
	}
}
header("location:../index.php");
