<?php
require 'connect.php';
session_start();
$target_dir = $_POST['targetDir'];
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$email =  explode("@", $_SESSION['Email']);
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "A fájl egy kép - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "A fájl nem egy kép.";
        $uploadOk = 0;
    }
}
// Check if file already exists
/*if (file_exists($target_file)) {
    echo "Sajnájuk,a fájl már létezik";
    $uploadOk = 0;
}*/
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000) {
    echo "Sajnáljuk, a kép túl nagy.Maximum 5MB";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
    echo "Sajnáljuk, csak JPG, JPEG, PNG fájlokat lehet feltölteni.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sajnáljuk, Sikertelen feltöltés.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        if($_POST['IsProfile'] == 'true'){
            rename($target_file, "../images/profile/".$email[0].".png");
            echo "A képet ". basename( $_FILES["fileToUpload"]["name"]). " sikeressen föltöltötted.";
            include "sqlfunctions.php";
            sqlupdate($connection,'users','profile_image','images/profile/'.$email[0].'.png');
            $_SESSION['ProfileImage'] = 'images/profile/'.$email[0].'.png';
            header("location:../profile.php");
            die();
        }else if($_POST['IsProfile'] == 'false'){
             $fileName = md5(date('Y-m-d H:i:s:u'));
             rename($target_file, $target_dir.$fileName.".png");


            /*require "sendMessage.php?message='images//chatImages//".$_POST['FileName']."'&taker=".$_POST['taker'];*/

            //require "sendMessage.php";
            $sql = "INSERT INTO chat (sender,taker,message,IsImage) VALUES ({$_SESSION['UserID']},{$_POST['taker']},'".str_replace('../', '', $target_dir).$fileName.'.png'."',true)";
            $connection->query($sql);
            die();
        }


    } else {
        //echo "Sajnáljuk, hiba lépet fel a feltöltésben.".$_FILES["fileToUpload"];
        var_dump($_FILES["fileToUpload"]);

        //header("location:../profile.php");
    }
}
?>
